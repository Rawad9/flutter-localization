import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

void main() {
  runApp(EasyLocalization(
    child: MyApp(),
    path: 'assets/',
    saveLocale: true,
    supportedLocales: [Locale('en', 'US'), Locale('ar', 'AR')],
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Title'.tr().toString()),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text(':)'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ExpansionTile(
              title: Text(
                'Language'.tr().toString(),
                style: TextStyle(fontSize: 17),
              ),
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                      child: Container(
                        height: 40,
                        width: double.infinity,
                        child: RaisedButton(
                          onPressed: () {
                            setState(() {
                              EasyLocalization.of(context).locale =
                                  Locale('en', 'US');
                              Navigator.pop(context);
                            });
                          },
                          child: Text('EN'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
                      child: Container(
                        height: 40,
                        width: double.infinity,
                        child: RaisedButton(
                          onPressed: () {
                            setState(() {
                              EasyLocalization.of(context).locale =
                                  Locale('ar', 'AR');
                              Navigator.pop(context);
                            });
                          },
                          child: Text('AR'),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
      body: Center(
        child: Text(
          'Translate Me'.tr().toString(),
          style: TextStyle(fontSize: 30.0),
        ),
      ),
    );
  }
}
